let http = require("http");
let port=3000;
http.createServer(function(request,response){

	if(request.url=='/login'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('you are in the login page');
	    }

	else{
	  	response.writeHead(404,{'Content-Type': 'text/plain'});
	  	response.end('This page does not exist');
	    }

}).listen(port);

console.log("server is successfully running at localhost:3000");