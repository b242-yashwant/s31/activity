/*
  3. The questions are as follows:
- What directive is used by Node.js in loading the modules it needs?
ans-require

- What Node.js module contains a method for server creation?
ans.http

- What is the method of the http object responsible for creating a server using Node.js?
ans.creatServer

- What method of the response object allows us to set status codes and content types?
response.Head

- Where will console.log() output its contents when run in Node.js?
in terminal

- What property of the request object contains the address's endpoint?
response.end()


*/